<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class ResultController extends AbstractController
{
    /**
     * @Route("/result", name="result")
     *
     * @param SessionInterface $session
     * @return RedirectResponse|Response
     */
    public function index(SessionInterface $session)
    {
        /**
         * Get result data from the session and pass to the result view
         */ 
        $dataResult = $session->get('dataResult');

        if (isset($_SERVER['HTTP_REFERER']) &&
            (
                $_SERVER['HTTP_REFERER'] == 'https://'.$_SERVER['HTTP_HOST'].'/' ||
                $_SERVER['HTTP_REFERER'] == 'http://'.$_SERVER['HTTP_HOST'].'/'
            )) {
            return $this->render(
                'result/index.html.twig',
                ['result' => $dataResult]
            );
        }

        return $this->redirectToRoute('homepage');
    }
}
