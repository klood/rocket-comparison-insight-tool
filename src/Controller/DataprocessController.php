<?php

namespace App\Controller;


use App\Service\Calculator;
use App\Service\HubSpotForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Class DataprocessController
 * @package App\Controller
 */
class DataprocessController extends AbstractController
{
    /**
     * @Route("/dataprocess", name="dataprocess")
     *
     * @param  Request          $request
     * @param  SessionInterface $session
     * @param  Calculator       $calculator
     * @param  HubSpotForm      $hubSpotForm
     * @param  Filesystem       $filesystem
     * @return JsonResponse
     */
    public function ajaxCall(
        Request $request,
        SessionInterface $session,
        Calculator $calculator,
        HubSpotForm $hubSpotForm,
        Filesystem $filesystem
    ) {
        if (is_null($userAnswers = json_decode($request->request->get('userScore')))) {
            return new JsonResponse(
                [
                    'status' => 'Error',
                    'message' => 'Error'
                ],
                400
            );
        }

        if (is_null($userData = json_decode($request->request->get('userData')))) {
            return new JsonResponse(
                [
                    'status' => 'Error',
                    'message' => 'Error'
                ],
                400
            );
        }

//        $filesystem->dumpFile(
//            'userData.json',
//            $request->request->get('userData')
//        );
//        $filesystem->dumpFile(
//            'userScore.json',
//            $request->request->get('userScore')
//        );

        $calculator->calculate($userAnswers);

        /**
        * Send data to hubspot by hubspot form api
        * Ref: https://developers.hubspot.com/docs/methods/forms/submit_form
        */
        $hubspotutk = '$_COOKIE'; //grab the cookie from the visitors browser.
        $ip_addr = $_SERVER['REMOTE_ADDR']; //IP address too.
        $hs_context = [
            'hutk' => $hubspotutk,
            'ipAddress' => $ip_addr,
            'pageUrl' => $_SERVER['SERVER_NAME'],
            'pageName' => 'Rocket | Result WMS Comparison Insight Tool'
        ];
        $hs_context_json = json_encode($hs_context);

        //Need to populate these variable with values from the form.
        $str_post = "firstname=" . urlencode($userData->name)
            . "&lastname="  .urlencode($userData->surname)
            . "&email=" . urlencode($userData->email)
            . "&message=" . $calculator->message
            . "&hs_context=" . urlencode($hs_context_json); //Leave this one be

        //replace the values in this URL with your portal ID and your form GUID
        $endpoint = $this->getParameter('wms.forms.questions');

        $hubSpotForm->post($str_post, $endpoint);

        $session->set('dataResult', $calculator->result);

        return new JsonResponse(
            [
//                'debug' => (array)$calculator,
                'status' =>  $calculator->result
            ],
            200
        );
    }
}
