<?php

namespace App\Service;


/**
 * Class CalculatorGapsUnit
 * @package App\Service
 */
class CalculatorGapsUnit extends CalculatorUnit
{
    /**
     * @param $value
     * @param $key
     */
    public function addValue($value, $key)
    {
        $this->array[$key] = $value;
        $this->total = $this->total + $value;
        if ($value < 0) {
            $this->count++;
        }
    }
}
