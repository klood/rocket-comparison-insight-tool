<?php

namespace App\Service;


/**
 * Class CalculatorScore
 * @package App\Service
 */
class CalculatorScore
{
    /**
     * @var CalculatorUnitInterface
     */
    public $room;

    /**
     * @var CalculatorUnitInterface
     */
    public $warehouse;

    /**
     * @var CalculatorUnitInterface
     */
    public $basic;

    /**
     * @var CalculatorUnitInterface
     */
    public $advanced;

    /**
     * CalculatorScore constructor.
     *
     * @param CalculatorUnitInterface $room
     * @param CalculatorUnitInterface $warehouse
     * @param CalculatorUnitInterface $basic
     * @param CalculatorUnitInterface $advanced
     */
    public function __construct(
        CalculatorUnitInterface $room,
        CalculatorUnitInterface $warehouse,
        CalculatorUnitInterface $basic,
        CalculatorUnitInterface $advanced
    ) {
        $this->room = $room;
        $this->warehouse = $warehouse;
        $this->basic = $basic;
        $this->advanced = $advanced;
    }
}
