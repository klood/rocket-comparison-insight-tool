<?php

namespace App\Service;


/**
 * Class CalculatorUnit
 * @package App\Service
 */
class CalculatorUnit implements CalculatorUnitInterface
{
    /**
     * @var array
     */
    public $array = [];

    /**
     * @var int
     */
    public $total = 0;

    /**
     * @var int
     */
    public $count = 0;

    /**
     * @param $value
     * @param $key
     */
    public function addValue($value, $key)
    {
        $this->array[$key] = $value;
        $this->total = $this->total + $value;
        if ($value > 0) {
            $this->count++;
        }
    }
}
