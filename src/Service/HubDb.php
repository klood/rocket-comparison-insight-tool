<?php


namespace App\Service;


use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

/**
 * Class HubDb
 * @package App\Service
 */
class HubDb
{
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var ContainerBagInterface
     */
    protected $params;

    /**
     * @var array
     */
    protected $area = [];

    /**
     * HubDb constructor.
     * @param Filesystem $filesystem
     * @param ContainerBagInterface $params
     */
    public function __construct(Filesystem $filesystem, ContainerBagInterface $params)
    {
        $this->filesystem = $filesystem;
        $this->params = $params;
        
        $this->initCacheDir();
    }

    public function initCacheDir()
    {
        if (!$this->filesystem->exists($this->params->get('wms.local.cache.dir'))) {
            $this->filesystem->mkdir($this->params->get('wms.local.cache.dir'));
        }
    }
    
    /**
     * @param  string $file
     * @param  string $url
     * @return mixed
     */
    public function getFileOrUrl(string $file, string $url)
    {
        try {
            return json_decode(file_get_contents($file));
        } catch (\Throwable $e) {
            return $this->getAndCacheUrl($url, $file);
        }
    }

    /**
     * @param  string $url
     * @param  string $file
     * @return mixed
     */
    public function getAndCacheUrl(string $url, string $file)
    {
        $urlContent = file_get_contents($url);

        $this->filesystem->dumpFile(
            $file,
            $urlContent
        );

        return json_decode($urlContent);
    }

    public function dump(string $file, $data)
    {
        $this->filesystem->dumpFile(
            $file,
            $data
        );
    }

    /**
     * @return mixed
     */
    public function getQuestions()
    {
        return $this->getFileOrUrl(
            $this->params->get('wms.local.questions'),
            $this->params->get('wms.hubdb.questions')
        );
    }

    /**
     * @return mixed
     */
    public function getAndCacheQuestions()
    {
        return $this->getAndCacheUrl(
            $this->params->get('wms.hubdb.questions'),
            $this->params->get('wms.local.questions')
        );
    }

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->getFileOrUrl(
            $this->params->get('wms.local.area'),
            $this->params->get('wms.hubdb.area')
        );
    }

    /**
     * @return mixed
     */
    public function getAndCacheArea()
    {
        return $this->getAndCacheUrl(
            $this->params->get('wms.hubdb.area'),
            $this->params->get('wms.local.area')
        );
    }

    /**
     * @return array|mixed
     */
    public function getCompositeQuestions()
    {
        try {
            return json_decode(
                file_get_contents($this->params->get('wms.local.questions.composite')),
                true
            );
        } catch (\ErrorException $e) {
            return $this->getAndCacheCompositeQuestions();
        }
    }

    /**
     * @return array
     */
    public function getAndCacheCompositeQuestions()
    {
        $questions = $this->getAndCacheQuestions();

        $area = $this->getAndCacheArea();

        $this->area = $area->objects;

        $questionsArray = [];

        $questions = $questions->objects;

        foreach ($questions as $value) {
            $question = [];

            $question['questionId'] =  $value->id;
            $question['questionName'] = $value->values->{'1'};
            $question['catId'] = $value->values->{'2'}[0]->id;;


            $question['questionCat'] = $this->getCatName($question['catId']);

            $question['answer_0'] = isset($value->values->{'7'}) ? $value->values->{'7'} : "";
            $question['answer_1'] = isset($value->values->{'8'}) ? $value->values->{'8'} : "";
            $question['answer_2'] = isset($value->values->{'9'}) ? $value->values->{'9'} : "";
            $question['answer_3'] = isset($value->values->{'10'}) ? $value->values->{'10'} : "";

            $questionsArray[] = $question;
        }

        $this->filesystem->dumpFile(
            $this->params->get('wms.local.questions.composite'),
            json_encode($questionsArray)
        );

        return $questionsArray;
    }

    /**
     * @return array|mixed
     */
    public function getFormula()
    {
        try {
            return json_decode(
                file_get_contents($this->params->get('wms.local.formula')),
                true
            );
        } catch (\ErrorException $e) {
            return $this->getAndCacheFormula();
        }
    }

    /**
     * @return array[]
     */
    public function getFormulaByValue()
    {
        $formula = $this->getFormula();

        return [
            (string)$formula['met']['value'] => [
                'image' => $formula['met']['image'],
                'name' => 'met'
            ],
            (string)$formula['partially']['value'] => [
                'image' => $formula['partially']['image'],
                'name' => 'partially'
            ],
            (string)$formula['exceeded']['value'] => [
                'image' => $formula['exceeded']['image'],
                'name' => 'exceeded'
            ],
            (string)$formula['gaps']['value'] => [
                'image' => $formula['gaps']['image'],
                'name' => 'gaps'
            ]
        ];
    }

    /**
     * @return array[]
     */
    public function getAndCacheFormula()
    {
        // Formula Raw
        $formulaRaw = json_decode(
            file_get_contents($this->params->get('wms.hubdb.formula'))
        );

        // Formula Array
        $formula = [
            'met' => [
                'value' => $formulaRaw->objects[0]->values->{'4'},
                'image' => $formulaRaw->objects[0]->values->{'5'}->url
            ],
            'partially' => [
                'value' => $formulaRaw->objects[1]->values->{'4'},
                'image' => $formulaRaw->objects[1]->values->{'5'}->url
            ],
            'exceeded' => [
                'value' => $formulaRaw->objects[2]->values->{'4'},
                'image' => $formulaRaw->objects[2]->values->{'5'}->url
            ],
            'gaps' => [
                'value' => $formulaRaw->objects[3]->values->{'4'},
                'image' => $formulaRaw->objects[3]->values->{'5'}->url
            ]
        ];

        $this->filesystem->dumpFile(
            $this->params->get('wms.local.formula'),
            json_encode($formula)
        );

        return $formula;
    }

    /**
     * @return mixed
     */
    public function getFunctionalCapability()
    {
        return $this->getFileOrUrl(
            $this->params->get('wms.local.functional'),
            $this->params->get('wms.hubdb.functional')
        );


    }

    /**
     * @return mixed
     */
    public function getAndCacheFunctionalCapability()
    {
        return $this->getAndCacheUrl(
            $this->params->get('wms.hubdb.functional'),
            $this->params->get('wms.local.functional')
        );
    }

    /**
     * @return mixed
     */
    public function getPlatformCapability()
    {
        return $this->getFileOrUrl(
            $this->params->get('wms.local.platform'),
            $this->params->get('wms.hubdb.platform')
        );
    }

    /**
     * @return mixed
     */
    public function getAndCachePlatformCapability()
    {
        return $this->getAndCacheUrl(
            $this->params->get('wms.hubdb.platform'),
            $this->params->get('wms.local.platform')
        );
    }

    /**
     * @return string[]
     */
    public function getDenyList()
    {
        try {
            return json_decode(
                file_get_contents($this->params->get('wms.local.denylist')),
                true
            );
        } catch (\ErrorException $e) {
            return $this->getAndCacheDenyList();
        }
    }

    /**
     * @return string[]
     */
    public function getAndCacheDenyList()
    {
        // Formula Raw
        $denyListRaw = json_decode(
            file_get_contents($this->params->get('wms.hubdb.denylist'))
        );

        $list = [];
        foreach ($denyListRaw->objects as $object) {
            $list[] = $object->values->{'1'};
        }

        $this->filesystem->dumpFile(
            $this->params->get('wms.local.denylist'),
            json_encode($list)
        );

        return $list;
    }

    /**
     * @param  $catId
     * @return string|null
     */
    protected function getCatName($catId)
    {
        foreach ($this->area as $singleCat) {
            $currentCat = $singleCat->id;
            if ($currentCat == $catId) {
                return $singleCat->values->{'1'};
            }
        }

        return null;
    }
}
