<?php


namespace App\Service;


/**
 * Class HubSpotForm
 * @package App\Service
 */
class HubSpotForm
{
    /**
     * @param  string $fields
     * @param  string $endpoint
     * @return mixed
     */
    public function post(string $fields, string $endpoint)
    {
        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_POST, true);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        @curl_setopt($ch, CURLOPT_URL, $endpoint);
        @curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response    = @curl_exec($ch); //Log the response from HubSpot as needed.
        $statusCode = @curl_getinfo($ch, CURLINFO_HTTP_CODE); //Log the response status code
        @curl_close($ch);

        return $statusCode;
    }
}